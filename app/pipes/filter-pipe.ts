import {Injectable, Pipe} from 'angular2/core';

/*
  Generated class for the FilterPipel pipe.

  See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
  Angular 2 Pipes.
*/
@Pipe({
  name: 'filter'
})
@Injectable()
export class FilterPipe {
  /*
    Takes a value and makes it lowercase.
   */
  transform(value, [name]) {
    return value.filter((item) => {
      return !name || item.name.indexOf(name) > -1;
    });
  }
}
