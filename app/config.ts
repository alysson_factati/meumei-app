
export class Config {
  static DEBUG = true;
  static LOCALSERVER = false;
  static SERVER = Config.LOCALSERVER? "http://localhost:1337" : "http://meumei.herokuapp.com";
}
