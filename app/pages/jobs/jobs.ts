import {Page, NavController, IonicApp, Alert} from 'ionic-angular';
import {JobSearchService} from '../../providers/job-search-service';
import {JobService} from '../../providers/job-service';
import {UserService} from '../../providers/user-service';
import {JobModel} from '../../models/job-model';
import {JobTypePage} from '../job_type/job_type';
import {CandidatesPage} from '../candidates/candidates';
import {ChatPage} from '../chat/chat';
import {FeedbackPage} from '../feedback/feedback';

@Page({
  providers: [JobService, JobSearchService],
  templateUrl: 'build/pages/jobs/jobs.html'
})
export class JobsPage {
  jobs = [];
  user;
  constructor(private userService: UserService, private jobSearchService: JobSearchService, private jobService: JobService, private nav: NavController, private app: IonicApp){
    this.listServices();
    this.user = UserService.current_user;
    console.log("inovapps_rest: Construtor da JobsPage");
  }

  listServices(){
    this.app.getComponent('loading').show();
    this.jobSearchService.list().then( data => {
      this.jobs = data;
      this.app.getComponent('loading').hide();
    });
  }

  deleteJob(job){
    let confirm = Alert.create({
      title: 'Exclusão de Serviço',
      message: 'Deseja mesmo excluir este serviço?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {}
        },
        {
          text: 'Excluir',
          handler: () => {
            this.app.getComponent('loading').show();
            this.jobService.delete(job._id).then( data => {
              this.jobs.splice( this.jobs.indexOf(job), 1 );
              this.app.getComponent('loading').hide();
            });
          }
        }
      ]
    });
    this.nav.present(confirm);
  }

  openJobCreatePage(){
    this.nav.push(JobTypePage, {jobs: this.jobs});
  }

  openJobSearch(job){
    this.nav.push(CandidatesPage, {job: job});
  }

  openJobDetails(job){
    this.nav.push(ChatPage, {job: job});
  }

  feedback(job){
    this.nav.push(FeedbackPage, {job: job});
  }
}
