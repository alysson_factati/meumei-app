import {Page, NavController, IonicApp, NavParams} from 'ionic-angular';
import {CnaeService} from '../../providers/cnae-service';
import {JobModel} from '../../models/job-model';
import {JobCreatePage} from '../job_create/job_create';
import {FilterPipe} from '../../pipes/filter-pipe';

@Page({
  providers: [CnaeService],
  pipes: [FilterPipe],
  templateUrl: 'build/pages/job_type/job_type.html'
})
export class JobTypePage {
  cnaes = [];
  jobs;
  name;
  loading;
  constructor(private cnaeService: CnaeService, private nav: NavController, private app: IonicApp, private params: NavParams){
    this.listServices();
    this.jobs = this.params.get('jobs');
  }

  listServices(){
    this.loading = this.app.getComponent('loading');
    this.loading.show();
    this.cnaeService.list().then( data => {
      this.cnaes = data
      this.loading.hide();
    });
  }

  openJobCreatePage(cnae){
    console.log(cnae);
    this.nav.push(JobCreatePage, {cnae: cnae, jobs: this.jobs});
  }
}
