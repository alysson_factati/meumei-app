import {Page, Alert, NavController} from 'ionic-angular';
import {UserService} from '../../providers/user-service';
import {IonicApp} from 'ionic-angular';
import {BarcodeScanner} from 'ionic-native';
import {DashboardPage} from '../dashboard/dashboard';
import {MeiPage} from '../mei/mei';
import {Config} from '../../config';
import {UserModel} from '../../models/user-model';

@Page({
  providers: [UserService],
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {

  on_first_page = true;
  on_last_page = false;
  isMei = false;
  constructor(private nav: NavController, private userService: UserService){
    console.log("inovapps_user: Home Page Constructor");
  }

  onChange(event){
    this.on_first_page = event.activeIndex == 0;
    this.on_last_page = event.activeIndex == 2;
  }

  updateMei(value){
    this.isMei = value;
  }

  login(){
    console.log("isMei: " + this.isMei)
    if(Config.DEBUG){
      this.userService.auth0(this.isMei).then( () => {
        this.nav.setRoot(DashboardPage)
        UserService.app.loginDone(this.isMei)
        //this.goToMainPage();
      })
    }
    else{
      this.userService.login(this.isMei).then( () => {
        console.log("meumei: Usuário autenticado! Abrindo Dashboard...");
        //this.goToMainPage();
      }).catch((error)=> console.dir(error))
    }

  }

  goToMainPage(){
    this.nav.pop();
    if(UserService.current_user.isMei && !UserService.current_user.isValidated){
      console.log("MEI!")
      this.nav.push(MeiPage)
    }
  }

  fakeUser(){
    UserService.current_user = new UserModel('fake_id', 'fake_token', false);
    UserService.current_user._id = '571bb4536150089f6da9befd';
    UserService.current_user.isMei = this.isMei;
    UserService.current_user.photo_url = 'https://api.adorable.io/avatars/200/meumei35@adorable.io.png';
    UserService.current_user.name = "Maria Pereira";
    UserService.current_user.email = "maria@silva.com";
    this.userService.saveUser((user) =>{
      console.log("Usuário salvo!");
    });
  };
}
