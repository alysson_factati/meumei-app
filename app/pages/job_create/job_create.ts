import {Page, NavController, NavParams, IonicApp} from 'ionic-angular';
import {JobService} from '../../providers/job-service';
import {UserService} from '../../providers/user-service';
import {CnaeService} from '../../providers/cnae-service';
import {JobModel} from '../../models/job-model';
import {CnaeModel} from '../../models/cnae-model';
import {Geolocation} from 'ionic-native';
import {JobsPage} from '../jobs/jobs';

@Page({
  providers: [JobService, CnaeService, UserService],
  templateUrl: 'build/pages/job_create/job_create.html'
})
export class JobCreatePage {
  job_type;
  job = new JobModel();
  jobs = [];
  constructor(private jobService: JobService, private cnaeService: CnaeService,
              private nav: NavController, private params: NavParams, private app: IonicApp){
    this.job_type = this.params.get('cnae');
  }

  createService(){
    this.app.getComponent('loading').show();
    Geolocation.getCurrentPosition().then(data => {
      this.job.cnae_code = this.job_type.code;
      this.job.cnae_name = this.job_type.name;
      this.job.location = [data.coords.longitude, data.coords.latitude];
      this.job.creator_id = UserService.current_user._id;
      this.job.creator_name = UserService.current_user.name;
      this.job.creator_photo = UserService.current_user.photo_url;
      this.jobService.save(this.job).then( data => {
        let jobs = this.params.get('jobs') || [];
        jobs.push(this.job);
        this.app.getComponent('loading').hide();
        this.nav.setRoot(JobsPage);
      } );
    });
  }

  back(){
    this.nav.pop();
  }
}
