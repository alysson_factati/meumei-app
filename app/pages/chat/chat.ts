declare var io:any;
import {Page, NavParams} from 'ionic-angular';
import {Http} from "angular2/http";
import {NgZone} from "angular2/core";
import {UserService} from "../../providers/user-service";
import {Config} from "../../config";

@Page({
  templateUrl: 'build/pages/chat/chat.html'
})
export class ChatPage {
  messages; zone; socket; message; job; user;
  constructor(http: Http, private params: NavParams) {
    this.job = params.get("job");
    this.user = UserService.current_user;
    this.zone = new NgZone({enableLongStackTrace: false});

    http.get(Config.SERVER + "/api/chat/" + this.job._id)
    .map(res => res.json())
    .subscribe(
      data => {
        this.messages = data;
    }, (error) => {
        console.log(JSON.stringify(error));
    });

    this.socket = io(Config.SERVER);
    this.socket.on(this.job._id, (msg) => {
      console.log("Mensagem recebida! " + msg);
        this.zone.run(() => {
            this.messages.push(msg);
        });
    });
  }

  send() {
    let message = {job: this.job._id, message: this.message, user: UserService.current_user};
    if(message && message.message != "") {
        this.socket.emit("chat_message", message);
    }
    this.message = "";
  }
}
