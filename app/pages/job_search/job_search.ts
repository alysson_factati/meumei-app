import {Page, IonicApp} from 'ionic-angular';
import {UserService} from '../../providers/user-service';
import {NearbyService} from '../../providers/nearby-service';
import {DatePipe} from '../../pipes/date-pipe';

@Page({
  providers: [NearbyService, UserService],
  pipes: [DatePipe],
  templateUrl: 'build/pages/job_search/job_search.html'
})
export class JobSearchPage {
  user; loading;
  jobs = [];
  current_job;
  constructor(private nearbyService: NearbyService, private userService: UserService, private app: IonicApp){
    this.user = UserService.current_user;
    this.loadJobs();
  }

  loadJobs(){
    this.loading = true;
    this.app.getComponent('loading').show();
    this.userService.updateUserLocation().then((user) => {
      this.nearbyService.listNearby().then( (jobs) =>{
        this.jobs = jobs.result;
        this.current_job = this.jobs[0];
        console.log("Trabalhos recebidos: " + jobs.result.length);
        console.dir(this.current_job);
        this.app.getComponent('loading').hide();
        this.loading = false;
      });
    });
  }

  acceptJob(){
    let page = this;
    this.nearbyService.acceptJob(this.current_job.obj).then((resultado) => {
      console.log("Serviço aceito!");
      this.jobs.splice(0,1);
      this.current_job = this.jobs[0];
    });

  }

  refuseJob(){
    this.nearbyService.refuseJob(this.current_job.obj).then((resultado) => {
      console.log("Serviço recusado!");
    });
    this.jobs.splice(0,1);
    this.current_job = this.jobs[0];
  }
}
