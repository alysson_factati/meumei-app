import {Injectable} from 'angular2/core';
import {Facebook, Geolocation} from 'ionic-native';
import {UserModel} from '../models/user-model';
import {RESTService} from './rest-service';
import {Http} from 'angular2/http';

declare var Auth0: any;
declare var Auth0Lock: any;

@Injectable()
export class UserService extends RESTService<UserModel>{

  static current_user: UserModel;
  static app;
  lock = new Auth0Lock('FC61uqavSQvYt0pFb94YRSwYdWdp28XV', 'melomario.auth0.com');

  constructor(http: Http){
    super(http);
    this.loadUser();
  }

  auth0(isMei: any){
    var promiseResult;
    var promiseError;
    let promise = new Promise<UserModel>((resolve, reject) => {promiseResult = resolve; promiseError = reject});

    this.lock.show({
      dict: 'pt-br',
      icon: 'img/icon.png',
      socialBigButtons: true,
      disableResetAction: true,
      closable: false,
      sso: false,
      loginAfterSignup: true,
      authParams: {
        scope: 'openid offline_access',
        device: 'Mobile device'
      },
      additionalSignUpFields: [{
        name: "full_name",
        placeholder: "Informe seu nome"
      }]
    }, (err, profile, token, accessToken, state, refreshToken) => {
      if (err) {
        alert(err);
      }
      // If authentication is successful, save the items
      // in local storage
      console.log(profile)
      UserService.current_user = new UserModel(profile.user_id, accessToken, isMei);
      UserService.current_user.name = profile.nickname;
      UserService.current_user.email = profile.email;
      UserService.current_user.photo_url = profile.picture;
      this.saveUser(promiseResult);
    });

    return promise;
  }

  getRouteName(){
    return 'users';
  }

  isLoggedIn(){
    return UserService.current_user;
  }

  loadUser(){
    if(!UserService.current_user && window.localStorage["current-user"])
      UserService.current_user = JSON.parse(window.localStorage["current-user"]);
  }

  updateUserLocation(): Promise<UserModel>{
      var promiseResult;
      var promiseError;
      let promise = new Promise<UserModel>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
      Geolocation.getCurrentPosition().then(data => {
        UserService.current_user.location = [data.coords.longitude, data.coords.latitude];
        this.saveUser(promiseResult);
        console.log ("inovapps_location: " + data);
      }).catch(error => {
        console.log ("inovapps_location: " + error.code);
        console.log ("inovapps_location: " + error.message);
        promiseError(UserService.current_user);
      });

      return promise;
  }

  logout(){
    window.localStorage["current-user"] = null;
    UserService.current_user = null;
  }

  login(isMei): Promise<UserModel>{
    var promiseResult;
    var promiseError;
    let promise = new Promise<UserModel>((resolve, reject) => {promiseResult = resolve; promiseError = reject});
    let result = Facebook.login(['public_profile', 'email']);
    console.log("meumei: Logando Usuário");
    result.then((response)=>{
      UserService.current_user = new UserModel(response.authResponse.userID, response.authResponse.accessToken, isMei);
      console.log("meumei: Usuário logado! Obtendo dados...");
      Facebook.api('me?fields=name,email', ['public_profile', 'email']).then((response) => {
        UserService.current_user.name = response.name;
        UserService.current_user.email = response.email;
        console.log("meumei: Dados Obtidos! Obtendo Fotos...");
        Facebook.api('me/picture?redirect=false&height=180&width=180', []).then((pic_response) => {
          UserService.current_user.photo_url = pic_response.data.url;
          console.log("meumei: Fotos obtidas!");
          this.saveUser(promiseResult);
        }).catch( (error) => promiseError(error.errorMessage));
      }).catch( (error) => promiseError(error.errorMessage));

    }).catch((error) => {
      promiseError(error.errorMessage);
    });

    return promise;
  }

  saveUser(callback){
    console.log("meumei: Salvando usuário...");
    this.save(UserService.current_user).then((user) => {
      UserService.current_user = user;
      UserService.StoreUser();
      UserService.app.refreshUser();
      console.log("meumei: Usuário salvo!");
      callback(user);
    });
  }

  static StoreUser(){
    window.localStorage["current-user"] = JSON.stringify(UserService.current_user);
  }
}
