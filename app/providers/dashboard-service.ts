import {Injectable} from 'angular2/core';
import {JobModel} from '../models/job-model';
import {RESTService} from './rest-service';
import {UserService} from './user-service';
import {Http} from 'angular2/http';

@Injectable()
export class DashboardService extends RESTService<JobModel>{
  getRouteName(): string{
    return "dashboard";
  }

  constructor(http: Http){
    super(http);
  }

}
