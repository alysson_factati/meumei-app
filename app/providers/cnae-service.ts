import {Injectable} from 'angular2/core';
import {CnaeModel} from '../models/cnae-model';
import {RESTService} from './rest-service';
import {Http} from 'angular2/http';

@Injectable()
export class CnaeService extends RESTService<CnaeModel>{
  getRouteName(): string{
    return "cnae";
  }

  constructor(http: Http){
    super(http);
  }
}
